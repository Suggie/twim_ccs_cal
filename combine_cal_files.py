import os
import pandas as pd
from aux_scripts import get_files


def combine_files(dirpath, gas_type='n2', cross_val_file=False):
    """
    combine the calibrated output files to get three files: params, diagnostisc, and calibrated data
    :param dirpath: dirpath
    :param outname: outname
    :return: none
    """

    ccs_database_df = pd.read_csv(os.path.join(os.getcwd(), "CCSDatabase\ccsdatabse_positive.csv"))

    params_string = ''
    diagnos_string = ''
    caldata_string = ''

    files = get_files(dirpath, endid='.csv')

    for index, file in enumerate(files):

        file_str = str(file)

        print(file_str)

        if cross_val_file:
            file_str, cross_val_str = file_str.split('#')
            cross_val_id = cross_val_str.split('.csv')[0]

        wh = float(file_str.split('wh_')[1].split('_')[0])
        wv = float(file_str.split('wv_')[1].split('_')[0])

        with open(os.path.join(dirpath, file), 'r') as output_file:

            output_read = output_file.read().splitlines()

            for num in range(len(output_read)):

                if output_read[num].startswith('[PARAMETERS]'):
                    param_start_ind = num+2
                if output_read[num].startswith('[DIAGNOSTICS]'):
                    param_end_ind = num-1
                    diagnos_start_ind = num+2
                if output_read[num].startswith('[CALIBRATED DATA]'):
                    diagnos_end_ind = num-1
                    caldata_start_id = num+2

            caldata_end_id = len(output_read)

            for ind in range(param_start_ind, param_end_ind, 1):
                if cross_val_file:
                    params_string += '{},{},{},{}\n'.format(str(wh), str(wv), cross_val_id, output_read[ind])
                else:
                    params_string += '{},{},{}\n'.format(str(wh), str(wv), output_read[ind])
            for ind in range(diagnos_start_ind, diagnos_end_ind, 1):
                spec_chars = output_read[ind].split(',')[0]
                oligo_num = spec_chars.split('_')[-1]
                spec_id = '_'.join(x for x in spec_chars.split('_')[:-1])
                rest_diagnos_str = ','.join(x for x in output_read[ind].split(',')[1:])
                if cross_val_file:
                    diagnos_string += '{},{},{},{},{},{}\n'.format(str(wh), str(wv), cross_val_id, spec_id, oligo_num,
                                                                   rest_diagnos_str)
                else:
                    diagnos_string += '{},{},{},{},{}\n'.format(str(wh), str(wv), spec_id, oligo_num, rest_diagnos_str)
            for ind in range(caldata_start_id, caldata_end_id, 1):
                spec_chars = output_read[ind].split(',')[0]
                oligo_num = spec_chars.split('_')[-1]
                spec_id = '_'.join(x for x in spec_chars.split('_')[:-1])
                charge_ = int(output_read[ind].split(',')[2])
                df_select = ccs_database_df[(ccs_database_df['id'] == spec_id) & (
                            ccs_database_df['n_oligomers'] == int(oligo_num)) & (ccs_database_df['z'] == charge_)]
                pub_ccs = df_select['ccs_'+gas_type].values[0] * 100
                pred_ccs = float(output_read[ind].split(',')[-2])
                ccs_deviation = (pred_ccs - pub_ccs)*100/pub_ccs
                rest_caldata_str = ','.join(x for x in output_read[ind].split(',')[1:])
                if cross_val_file:
                    caldata_string += '{},{},{},{},{},{},{},{}\n'.format(str(wh), str(wv), cross_val_id, spec_id, oligo_num,
                                                                   pub_ccs, rest_caldata_str, str(ccs_deviation))
                else:
                    caldata_string += '{},{},{},{},{},{},{}\n'.format(str(wh), str(wv), spec_id, oligo_num, pub_ccs,
                                                                rest_caldata_str, str(ccs_deviation))

    if cross_val_file:
        params_header = 'wh,wv,cross_val_id,g,dg,a,da,c,dc,t0,dt0\n'
        diagnos_header = 'wh,wv,cross_val_id,id,oligo_num,mass,z,mobility,alpha,gamma,model_vel,exp_vel,error_%\n'
        caldata_header = 'wh,wv,cross_val_id,id,oligo_num,ref_ccs,mass,z,drift,ccs,ccs_std_dev,ccs_dev_percent\n'
    else:
        params_header = 'wh,wv,g,dg,a,da,c,dc,t0,dt0\n'
        diagnos_header = 'wh,wv,id,oligo_num,mass,z,mobility,alpha,gamma,model_vel,exp_vel,error_%\n'
        caldata_header = 'wh,wv,id,oligo_num,ref_ccs,mass,z,drift,ccs,ccs_std_dev,ccs_dev_percent\n'

    with open(os.path.join(dirpath, 'combined_params.csv'), 'w') as outfile:
        outfile.write(params_header+params_string)
        outfile.close()

    with open(os.path.join(dirpath, 'combined_diagnos.csv'), 'w') as outfile:
        outfile.write(diagnos_header+diagnos_string)
        outfile.close()

    with open(os.path.join(dirpath, 'combined_caldata.csv'), 'w') as outfile:
        outfile.write(caldata_header+caldata_string)
        outfile.close()


if __name__=='__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\output2"
    combine_files(dirpath, gas_type='n2', cross_val_file=True)

    # working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\_norad\vary_a\_crosscls"
    #
    # cls_lab = ['denat', 'metab', 'natprot', 'peptide']
    #
    # for cls1 in cls_lab:
    #     for cls2 in cls_lab:
    #         dirpath = os.path.join(working_dir, r'Cal'+cls1+'_'+cls2+'/output')
    #         combine_files(dirpath, cross_val_file=False)

    # working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_subcls\bsa_reversepeptides"
    #
    # class_lab = ['all', 'denat', 'metab', 'natprot', 'peptide']
    #
    # for cls in class_lab:
    #     dirpath = os.path.join(working_dir, r'Cal_' + cls + '\output')
    #     combine_files(dirpath, cross_val_file=False)