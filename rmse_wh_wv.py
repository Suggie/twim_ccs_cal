import os
import pandas as pd
import numpy as np
from aux_scripts import rms_error


def gen_ccs_rmse_wh_wv(caldata_fpath):
    """
    generate ccs rmse for each wh and wv including all ions and total rmse of all ions at all conditions
    :param caldata_fpath: calibrated data output
    :return: None. rmse for each wh wv conditon stored in a file
    """

    dirpath, fname = os.path.split(caldata_fpath)

    df = pd.read_csv(caldata_fpath)

    total_rmse = rms_error(df['ref_ccs'].values, df['ccs'].values)

    df['wh_wv_key'] = df['wh'].map(str) + '_' + df['wv'].map(str)
    uniq_wh_wv_arr = np.unique(df['wh_wv_key'].values)
    uniq_wh_wv_rmse = []
    for ind, uniq_wh_wv in enumerate(uniq_wh_wv_arr):
        df_uniq_wh_wv = df[df['wh_wv_key'] == uniq_wh_wv]
        uniq_wh_wv_rmse.append(rms_error(df_uniq_wh_wv['ref_ccs'].values, df_uniq_wh_wv['ccs'].values))

    data_string = ''
    for index, (wh_wv, rmse) in enumerate(zip(uniq_wh_wv_arr, uniq_wh_wv_rmse)):
        wh_, wv_ = wh_wv.split('_')
        data_string += '{},{},{}\n'.format(wh_, wv_, str(rmse))

    tot_rmse_str = '$total_rmse,{}\n'.format(str(total_rmse))

    header = 'wh,wv,ccs_rmse\n'



    with open(os.path.join(dirpath, 'caldata_wh_wv_rmse.csv'), 'w') as outfile:
        outfile.write(tot_rmse_str+header+data_string)
        outfile.close()

if __name__ == '__main__':

    caldata = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\fix_a\all_ion\leaveonespec\output\combined_caldata.csv"
    gen_ccs_rmse_wh_wv(caldata)