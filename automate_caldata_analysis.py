from aux_scripts import get_files
import os
from rmse_wh_wv import gen_ccs_rmse_wh_wv
from species_ccsdev_ccsrmse import gen_species_ccs_dev_rmse, gen_ccs_dev_array_stats

working_dir = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\output2"
caldata_fpath = os.path.join(working_dir, 'combined_caldata.csv')
gen_ccs_rmse_wh_wv(caldata_fpath)
gen_species_ccs_dev_rmse(caldata_fpath)
gen_ccs_dev_array_stats(caldata_fpath)

# working_dir = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\_box_whisk_new_smallmol_data\sub_cls\stats"
# caldata_list = get_files(working_dir, endid='.csv')
# for ind, cal_data_f in enumerate(caldata_list):
#     caldata_fpath = os.path.join(working_dir, cal_data_f)
#     # gen_ccs_rmse_wh_wv(caldata_fpath)
#     # gen_species_ccs_dev_rmse(caldata_fpath)
#     gen_ccs_dev_array_stats(caldata_fpath)

#
# cls_lab = ['denat', 'metab', 'natprot', 'peptide']
#
# for cls1 in cls_lab:
#     for cls2 in cls_lab:
#         caldata_fpath = os.path.join(working_dir, r'Cal'+cls1+'_'+cls2+'/output/combined_caldata.csv')
#         gen_ccs_rmse_wh_wv(caldata_fpath)
#         gen_species_ccs_dev_rmse(caldata_fpath)


# working_dir = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\blend_rad\metab_smallmol"
#
# sub_dirs = [r'all\output', r'leaveoneion\output', r'leaveonespec\output']
#
# for dir_ in sub_dirs:
#
#     dirpath = os.path.join(working_dir, dir_)
#     caldata_fpath = os.path.join(dirpath, 'combined_caldata.csv')
#     gen_ccs_rmse_wh_wv(caldata_fpath)
#     gen_species_ccs_dev_rmse(caldata_fpath)
#     gen_ccs_dev_array_stats(caldata_fpath)

# working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_subcls\bsa_reversepeptides"
#
# class_lab = ['all', 'denat', 'metab', 'natprot', 'peptide']
#
# for cls in class_lab:
#     dirpath = os.path.join(working_dir, r'Cal_' + cls + '\output')
#     caldata_fpath = os.path.join(dirpath, 'combined_caldata.csv')
#     gen_ccs_rmse_wh_wv(caldata_fpath)
#     gen_species_ccs_dev_rmse(caldata_fpath)
#     gen_ccs_dev_array_stats(caldata_fpath)
