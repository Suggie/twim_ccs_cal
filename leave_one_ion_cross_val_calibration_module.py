import os
import subprocess
import time
from aux_scripts import create_dirpath, string_to_batch_file, get_files


def make_input_files_and_run_calibration(all_ion_dirpath, outdir, cal_exec_file, cal_batch_fname, pressure, temperature,
                                         accuracy,
                     length=0.254, lambda_=0.012,
                     fix_params=None, run_cal=True):

    """
    read input file from all ion directory and make leave one ref file and unk file
    take wave height and wave velocity from the file name
    :param gauss_file_list: gauss file list
    :param dirpath: directory to save files
    :return:
    """

    print('Making input files now ...')

    ref_file_list = get_files(all_ion_dirpath, endid='.dat')

    batch_string = ''

    ref_dirpath = create_dirpath(os.path.join(outdir, 'ref_input'))
    unk_dirpath = create_dirpath(os.path.join(outdir, 'unk_input'))
    output_dirpath = create_dirpath(os.path.join(outdir, 'output'))

    for index, file in enumerate(ref_file_list):

        file_str = str(file).split('.dat')[0]
        file_chars = file_str.split('_')
        wh = float(file_chars[2])
        wv = float(file_chars[4])

        with open(os.path.join(all_ion_dirpath, file), 'r') as all_ion_file:

            all_ion_file_read = all_ion_file.read().splitlines()

            for num in range(len(all_ion_file_read)):

                ref_file_string = ''
                unk_file_string = ''

                unk_file_string += all_ion_file_read[num]
                unk_string_chars = all_ion_file_read[num].split()
                unk_id = '_'.join([unk_string_chars[0], unk_string_chars[2]])

                if num == 0:
                    ref_combo = all_ion_file_read[num+1:]
                else:
                    ref_combo = all_ion_file_read[:num] + all_ion_file_read[num+1:]

                for line in ref_combo:
                    ref_file_string += line + '\n'

                ref_fpath = os.path.join(ref_dirpath, 'ref_wh_'+str(wh)+'_wv_'+str(wv)+'#'+unk_id+'.dat')
                unk_fpath = os.path.join(unk_dirpath, 'unk_wh_'+str(wh)+'_wv_'+str(wv)+'#'+unk_id+'.dat')

                output_fpath = os.path.join(output_dirpath, 'output_wh_'+str(wh)+'_wv_'+str(wv)+'#'+unk_id+'.csv')

                with open(ref_fpath, 'w') as ref_file:
                    ref_file.write(ref_file_string)
                    ref_file.close()

                with open(unk_fpath, 'w') as unk_file:
                    unk_file.write(unk_file_string)
                    unk_file.close()


                batch_string_ = string_to_batch_file(cal_exec_file, ref_file=ref_fpath,
                                                     cal_input_file=unk_fpath,
                                                     output_file=output_fpath,
                                                     wave_height=wh,
                                                     wave_velocity=wv,
                                                     length=length,
                                                     pressure=pressure,
                                                     temperature=temperature,
                                                     lambda_=lambda_,
                                                     accuracy=accuracy,
                                                     fix_params=fix_params)

                batch_string += batch_string_

    with open(os.path.join(outdir, cal_batch_fname), 'w') as batch_file:
        batch_file.write(batch_string)
        batch_file.close()

    print('Finished making input files!')

    if run_cal:
        print('Running calibration now ... start the timer!')
        start_time = time.time()
        subprocess.run(os.path.join(outdir, cal_batch_fname))
        print('\n Finished! It took blimey ', round((time.time() - start_time)/60, 2), ' minutes')


if __name__=='__main__':

    all_ion_dpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\norad\metab\all\ref_input"
    outdir = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\norad\metab\leaveoneion"
    cal_exec_file = os.path.join(os.getcwd(), r"IMSCal\bin\TWaveCalibrate")
    fix_a_ = [['a', 1]]
    fix_c_ = [['c', 0]]
    fix_a_c_ = [['a', 1], ['c', 0]]
    fix_c_scale_a = [['c', 0], ['scale_a']]
    scale_a = [['scale_a']]
    scale_a_fix_t = [['scale_a'], ['t0', 0.00055]]
    fix_t_fix_c = [['t0', 0.00055], ['c', 0]]
    scale_a_fix_t_fix_c = [['t0', 0.00055], ['c', 0], ['scale_a']]
    make_input_files_and_run_calibration(all_ion_dirpath=all_ion_dpath, outdir=outdir, cal_exec_file=cal_exec_file,
                                         cal_batch_fname='TwaveCal_batchfile.bat', pressure=3.4, temperature=298,
                                         accuracy=3, fix_params=fix_c_scale_a, run_cal=False)