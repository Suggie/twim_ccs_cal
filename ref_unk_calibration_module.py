import os
import subprocess
import time
import pandas as pd
from aux_scripts import create_dirpath, get_cal_info_dict_from_gauss_flist, string_to_batch_file, correct_time_for_tof


def write_input_files_for_ref_and_unk(dirpath, gauss_fit_flist_ref, gauss_fit_flist_unk, cal_exec_file, cal_batch_fname,
                                      pressure, temperature, accuracy, length=0.254, lambda_=0.012, fix_params=None,
                                      run_cal=True):
    """
    write the input files for ref and unk and run calirbation
    :param dirpath: directory to save the files
    :param gauss_fit_flist_ref: gauss fit file list for ref ions
    :param gauss_fit_flist_unk: gauss fit file list for unk ions
    :param cal_exec_file: cal executable file
    :param cal_batch_fname: batch file name
    :param pressure: pressure in mbar
    :param temperature: temperature in kelvin
    :param accuracy: accuracy in %
    :param length: length of cell
    :param lambda_: wavelenth number
    :param fix_params: fix parameters lsit
    :param run_cal: bool value
    :return:None
    """

    ref_dirpath = create_dirpath(os.path.join(dirpath, 'ref_input'))
    unk_dirpath = create_dirpath(os.path.join(dirpath, 'unk_input'))
    output_dirpath = create_dirpath(os.path.join(dirpath, 'output'))

    ref_cal_dict = get_cal_info_dict_from_gauss_flist(gauss_fit_flist_ref, gas_type='n2', ref_ions=True)
    unk_cal_dict = get_cal_info_dict_from_gauss_flist(gauss_fit_flist_unk, gas_type='n2', ref_ions=False)

    gauss_df_0 = pd.read_csv(ref_cal_dict['gauss_fpath_list'][0])
    wh_conditions, wv_conditions = gauss_df_0.iloc[:, 0].values, gauss_df_0.iloc[:, 1].values

    # cal_dict_ref = dict()
    # cal_dict_unk = dict()

    cal_dict_dir_list = [['ref', ref_cal_dict, ref_dirpath], ['unk', unk_cal_dict, unk_dirpath]]

    batch_string_ = ''

    print('Now writing files ... ')

    for ind, (wv_ht, wv_vel) in enumerate(zip(wh_conditions, wv_conditions)):
        cal_input_fpath_dict = dict()
        for index, (cal_label, cal_dict, outdir) in enumerate(cal_dict_dir_list):
            input_string = ''
            for index_ in range(len(cal_dict['species'])):
                gauss_df = pd.read_csv(cal_dict['gauss_fpath_list'][index_])
                drift_time_ = \
                gauss_df[(gauss_df.iloc[:, 0] == wv_ht) & (gauss_df.iloc[:, 1] == wv_vel)].iloc[:, 4].values[0]
                corr_drift_time = correct_time_for_tof(drift_time_, cal_dict['mass'][index_],
                                                       cal_dict['charge'][index_], edc_constant=1.57)
                id_ = '_'.join([str(cal_dict['species'][index_]), str(cal_dict['oligomer'][index_])])
                line = '{} {} {} {} {}\n'.format(id_,
                                                 cal_dict['mass'][index_],
                                                 cal_dict['charge'][index_],
                                                 cal_dict['ccs'][index_],
                                                 corr_drift_time)
                input_string += line

            input_fname = cal_label + '_wh_' + str(wv_ht) + '_wv_' + str(wv_vel) + '.dat'
            input_fpath = os.path.join(outdir, input_fname)

            with open(input_fpath, 'w') as input_file:
                input_file.write(input_string)
                input_file.close()

            cal_input_fpath_dict[cal_label+'_input_fpath'] = input_fpath

        cal_output_fpath = os.path.join(output_dirpath, 'unk_wh_' + str(wv_ht) + '_wv_' + str(wv_vel) + '_output.csv')

        batch_line = string_to_batch_file(cal_executable_path=cal_exec_file,
                                          ref_file=cal_input_fpath_dict['ref_input_fpath'],
                                          cal_input_file=cal_input_fpath_dict['unk_input_fpath'],
                                          output_file=cal_output_fpath,
                                          wave_height=wv_ht,
                                          wave_velocity=wv_vel,
                                          length=length,
                                          pressure=pressure,
                                          temperature=temperature,
                                          lambda_=lambda_,
                                          accuracy=accuracy,
                                          fix_params=fix_params)
        batch_string_ += batch_line

    with open(os.path.join(dirpath, cal_batch_fname), 'w') as batch_file:
        batch_file.write(batch_string_)
        batch_file.close()

    print('Finished writing files ... ')


    if run_cal:
        print('Now running calibration ... start timer')
        start_time = time.time()
        subprocess.run(os.path.join(dirpath, cal_batch_fname))
        print('\n Finished! It took blimey ', round((time.time() - start_time) / 60, 2), ' minutes')



if __name__=='__main__':
    gauss_flist_ref = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\norad\peptide\gauss_file_list_for_cal.csv"
    gauss_flist_unk = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\norad\peptide\gauss_file_list_for_cal.csv"
    dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\norad\peptide\all"
    cal_exec_file = os.path.join(os.getcwd(), r"IMSCal\bin\TWaveCalibrate")
    cal_batch_name = 'TwaveCal_batchfile.bat'
    fix_a_ = [['a', 1]]
    fix_c_ = [['c', 0]]
    fix_a_c_ = [['a', 1], ['c', 0]]
    fix_c_scale_a = [['c', 0], ['scale_a']]
    scale_a = [['scale_a']]
    scale_a_fix_t = [['scale_a'], ['t0', 0.00055]]
    fix_t = [['t0',0.00055]]
    fix_t_fix_c = [['t0',0.00055], ['c', 0]]
    scale_a_fix_t_fix_c = [['t0',0.00055], ['c', 0], ['scale_a']]
    write_input_files_for_ref_and_unk(dirpath, gauss_flist_ref, gauss_flist_unk, cal_exec_file, cal_batch_name,
                                      pressure=3.4, temperature=298, accuracy=3, fix_params=fix_c_scale_a, run_cal=False)


    # class_lab = ['denat', 'metab', 'natprot', 'peptide']
    #
    # working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_crosscls"
    #
    # for cls in class_lab:
    #     ref_file = os.path.join(working_dir, cls+'_gauss_file_list_for_cal.csv')
    #     for cls2 in class_lab:
    #         unk_file = os.path.join(working_dir, cls2+'_gauss_file_list_for_cal.csv')
    #         outdir_name = 'Cal'+cls+'_'+cls2
    #         dirpath = os.path.join(working_dir, outdir_name)
    #
    #         write_input_files_for_ref_and_unk(dirpath, ref_file, unk_file, cal_exec_file, cal_batch_name,
    #                                           pressure=3.4, temperature=298, accuracy=3, fix_params=None,
    #                                           run_cal=True)

    # class_lab = ['all', 'denat', 'metab', 'natprot', 'peptide']
    #
    # working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_subcls\bsa_reversepeptides"
    # ref_file = os.path.join(working_dir, 'ref_gauss_file_list_for_cal.csv')
    #
    # for cls in class_lab:
    #     unk_file = os.path.join(working_dir, cls + '_gauss_file_list_for_cal.csv')
    #     outdir_name = 'Cal_' + cls
    #     dirpath = os.path.join(working_dir, outdir_name)
    #
    #     write_input_files_for_ref_and_unk(dirpath, ref_file, unk_file, cal_exec_file, cal_batch_name,
    #                                       pressure=3.4, temperature=298, accuracy=3, fix_params=None,
    #                                       run_cal=True)