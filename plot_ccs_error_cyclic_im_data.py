import os
import pandas as pd
from aux_scripts import get_files
from plotting_scripts import plot_bar_graph
import matplotlib.pyplot as plt

dirpath = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\__cyclic_IM_data\cyclic_calibration_species_noncal"

color_code_file = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\__cyclic_IM_data\color_code.csv"
color_df = pd.read_csv(color_code_file)

files = get_files(dirpath, endid='.dat')

for ind, file in enumerate(files):

    df = pd.read_csv(os.path.join(dirpath, file), sep='\s+', header=None)
    y_ = df.iloc[:, 2]
    y_err_list = list(df.iloc[:, 3].values)

    color_list = list(color_df['hex_code'].values)

    plot_bar_graph(y_=df.iloc[:, 2].values,
                   y_err=y_err_list,
                   x_labels=df.iloc[:, 0].values,
                   y_label='ccs_dev',
                   color_list=color_list)

    plt.savefig(os.path.join(dirpath, file+'ccsdev_2.pdf'))
    plt.close()