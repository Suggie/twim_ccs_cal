import os
import pandas as pd
import numpy as np
from aux_scripts import get_files
from plotting_scripts import construct_grid, plot_imshow_scale, plot_imshow, plot_bar_graph, \
    box_whisker_plot, plot_bar_graph_with_two_breaks_y_axis
import matplotlib.pyplot as plt


def plot_box_whisk_scatter(dataframe, dirpath, marker, color, xlim=None, ylim=None, y_int=1.0, pic_ext='.png'):
    """
    plot ccs deviation as scatter and plot box whisker
    :param dataframe: dataframe
    :param dirpath: directory to save
    :param data_key: data key to use
    :param marker: marker to use
    :param color: color to use
    :param xlim: x limit
    :param ylim: y limit
    :param y_int: y interval
    :return:
    """

    ref_ccs = dataframe['ref_ccs'].values
    pred_ccs = dataframe['ccs'].values

    ccs_dev = np.divide(np.multiply(np.subtract(pred_ccs, ref_ccs), 100), ref_ccs)

    box_whisker_plot(ccs_dev, color=color, marker=marker, multiple_data=False)

    plt.savefig(os.path.join(dirpath, 'ccs_dev_box_whisk'+pic_ext), dpi=500)
    plt.close()


def plot_box_whisk_multiple_data(dirpath, marker_list, color_list, xlim=None, ylim=None, y_int=1.0, pic_ext='.png'):
    """
    plot ccs deviation as scatter and plot box whisker
    :param df_list: df list
    :param dirpath: directory to save
    :param maker_list: marker list
    :param color_list: color list
    :param xlim: x limit
    :param ylim: ylimit
    :param y_int: y interval
    :param pic_ext:
    :return:
    """

    data_files = get_files(dirpath, endid='.csv')

    data_list = []

    for da_f in data_files:
        df = pd.read_csv(os.path.join(dirpath, da_f))
        ref_ccs = df['ref_ccs'].values
        pred_ccs = df['ccs'].values

        ccs_dev = np.divide(np.multiply(np.subtract(pred_ccs, ref_ccs), 100), ref_ccs)
        data_list.append(ccs_dev)

    box_whisker_plot(data_list, color_list, marker_list, multiple_data=True)
    plt.savefig(os.path.join(dirpath, 'ccs_dev_box_whisk_multiple' + pic_ext), dpi=500)
    plt.close()




def plot_wh_wv_ccs_rmse_contour(dataframe, dirpath, xkey, ykey, zkey, colormap, colormode, clim=None, scale=None,
                                vmin=None, vmax=None, pic_ext='.pdf'):
    """
    create contour plot of ccs rmse from pandas dataframe of wh_wv_rmse.csv
    :param dataframe: pandas dataframe
    :param dirpath: directory to save the picture
    :param pic_ext: picture extension
    :return: None
    """
    xval = dataframe[xkey].values
    yval = dataframe[ykey].values
    zval = dataframe[zkey].values

    data_grid = construct_grid(xval, yval, zval)

    if scale == None:
        plot_imshow(data_grid, x_val=xval, y_val=yval, x_key=xkey, y_key=ykey, z_key=zkey, colormap=colormap,
                    color_mode=colormode, clim=clim)
    else:
        plot_imshow_scale(data_grid, x_val=xval, y_val=yval, x_key=xkey, y_key=ykey, z_key=zkey, colormap=colormap,
                          vmin=vmin, vmax=vmax, scale=scale)

    plt.savefig(os.path.join(dirpath, 'wh_wv_ccs_rmse_' + str(scale) + '_' + pic_ext))
    plt.close()


def add_subclass_to_caloutput_data(dataframe):
    """
    use the dataframe from the output data and add the subclass
    :param dataframe:
    :return:
    """
    ccs_database_df = pd.read_csv(os.path.join(os.getcwd(), r"CCSDatabase\ccsdatabse_positive.csv"))
    ccs_database_df['spec_id'] = ccs_database_df['id'] + ccs_database_df['n_oligomers'].map(str) + ccs_database_df[
        'z'].map(str)

    dataframe = dataframe.sort_values(['mass', 'charge'], ascending=[True, True])
    dataframe['spec_id'] = dataframe['id'] + dataframe['oligo_num'].map(str) + dataframe['charge'].map(str)

    dataframe_class = []
    dataframe_color = []

    for spec_id in dataframe['spec_id'].values:
        dataframe_class.append(ccs_database_df[ccs_database_df['spec_id'] == spec_id]['subclass'].values[0])
        dataframe_color.append(ccs_database_df[ccs_database_df['spec_id'] == spec_id]['subclass_color_hex_code'].values[0])

    dataframe_class = np.array(dataframe_class)
    dataframe_color = np.array(dataframe_color)

    dataframe['subclass'] = dataframe_class
    dataframe['subclass_color'] = dataframe_color

    return dataframe



def plot_bar_graph_caloutput(dataframe, dirpath, y_key, y_err_key, y_label, plot_y_break=False, pic_ext='.pdf'):
    """
    create bar graph from the caloutput data
    :param dataframe: dataframe
    :param dirpath: directory where to save the picture
    :param y_key: y key
    :param yerr_key: y error key
    :param y_label: label for y
    :param pic_ext: extension for figure file
    :return: none
    """

    df = add_subclass_to_caloutput_data(dataframe)

    subclass_list = ['aminoacid', 'organic_compound', 'lipid', 'tripeptide', 'reverse_peptide', 'polyala',
                     'denature_protein', 'native_protein']

    x_label = []
    ydata = []
    y_err_data = []
    color_list = []

    for index, subclass in enumerate(subclass_list):
        if subclass in df['subclass'].values:
            df_subclass = df[df['subclass'] == subclass]
            if y_err_key:
                for ind, (id, oligo, ydata_, yerrdata_, color_) in enumerate(zip(df_subclass['id'], df_subclass['oligo_num'],
                                                                         df_subclass[y_key], df_subclass[y_err_key],
                                                                                 df_subclass['subclass_color'])):
                    ydata.append(ydata_)
                    y_err_data.append(yerrdata_)
                    label_string = '_'.join([id, str(oligo)])
                    x_label.append(label_string)
                    color_list.append(color_)

            else:
                for ind, (id, oligo, ydata_, color_) in enumerate(zip(df_subclass['id'], df_subclass['oligo_num'],
                                                              df_subclass[y_key], df_subclass['subclass_color'])):
                    ydata.append(ydata_)
                    label_string = '_'.join([id, str(oligo)])
                    x_label.append(label_string)
                    color_list.append(color_)

    plot_bar_graph(ydata, x_label, y_label, y_err_data, color_list=color_list)
    plt.savefig(os.path.join(dirpath, 'species_' + y_label + '_' + pic_ext))
    plt.close()

    # plot_bar_graph_with_two_breaks_y_axis(y_, y_err, x_labels, y_label, ylim1, ylim2, ylim3, color_list):
    if plot_y_break:
        plot_bar_graph_with_two_breaks_y_axis(ydata, y_err_data, x_label, y_label, (7, 9), (-4, 5), (-13, -7),
                                              color_list=color_list)
        plt.savefig((os.path.join(dirpath, 'species_' + y_label + '_y_break' + pic_ext)))
        plt.close()


def automate_plots(dirpath):
    """
    make all necessary plots
    :param dirpath: directory where files are and will be saved
    :return: none
    """
    # wh_wv_rmse_fpath = os.path.join(dirpath, 'caldata_wh_wv_rmse.csv')
    species_rmse_dev_fpath = os.path.join(dirpath, 'caldata_species_ccs_rmse_dev_combined_caldata.csv')
    # wh_wv_rmse_df = pd.read_csv(wh_wv_rmse_fpath, header=1)
    species_rmse_dev_df = pd.read_csv(species_rmse_dev_fpath)
    plot_bar_graph_caloutput(species_rmse_dev_df, dirpath,
                             y_key='ccs_dev_avg',
                             y_err_key='ccs_dev_std',
                             y_label='ccs_dev',
                             plot_y_break=True)
    # plot_bar_graph_caloutput(species_rmse_dev_df, dirpath,
    #                          y_key='ccs_rmse',
    #                          y_err_key=None,
    #                          y_label='ccs_rmse')
    # plot_wh_wv_ccs_rmse_contour(wh_wv_rmse_df, dirpath,
    #                             xkey='wh',
    #                             ykey='wv',
    #                             zkey='ccs_rmse',
    #                             colormap='bone_r',
    #                             colormode='notdiverge')


if __name__ == '__main__':

    # dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\rad\allion\leaveonespec_scalea\output"
    # automate_plots(dirpath)


    # working_dir = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\blend_rad\metab_smallmol"
    #
    # sub_dirs = [r'all\output', r'leaveoneion\output', r'leaveonespec\output']
    #
    # for dir_ in sub_dirs:
    #
    #     dirpath = os.path.join(working_dir, dir_)
    #     automate_plots(dirpath)



    # working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_crosscls"
    #
    # cls_lab = ['denat', 'metab', 'natprot', 'peptide']
    #
    # for cls1 in cls_lab:
    #     for cls2 in cls_lab:
    #         dirpath = os.path.join(working_dir, r'Cal' + cls1 + '_' + cls2 + '/output')
    #         automate_plots(dirpath)

    # working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_subcls\bsa_reversepeptides"
    #
    # cls_lab = ['all', 'denat', 'metab', 'natprot', 'peptide']
    #
    # for cls in cls_lab:
    #     dirpath = os.path.join(working_dir, r'Cal_'+cls+'/output')
    #     automate_plots(dirpath)



    # dirpath = r"C:\Users\sugyan\Documents\Processed data\071819_Nanodisc_peptide_datanalysis\CCS_rIAPP_lipids_keithsoft\iapp_output\vary_a\ref_output_he"
    # caldata_fname = 'combined_caldata.csv'
    # df = pd.read_csv(os.path.join(dirpath, caldata_fname))
    # plot_box_whisk_scatter(df, dirpath, marker='o', color='orangered')

    color_label_all = ['slateblue', 'royalblue', 'orangered', 'coral']
    # color_label_all = ['slateblue', 'orangered']
    # color_label_all = ['black', 'black', 'black', 'black', 'black', 'black', 'black', 'black']
    marker_all = ['o', 'o', 'o', 'o']
    # marker_all = ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o']

    # marker_all = ['o', 'o']
    # color_label_all = ['slateblue', 'orangered']

    dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\fig_data\fig2\denatprot\boxwhisk"
    plot_box_whisk_multiple_data(dirpath, marker_list=marker_all, color_list=color_label_all, pic_ext='.jpeg')