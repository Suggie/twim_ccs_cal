import os
import pandas as pd

def curate_combined_caldata_wh_wv(combined_cal_data_fpath, wh_wv_list):
    """
    create new combined caldata by using only specified wh and wv conditions
    :param combined_cal_data_fpath: combined caldata file path
    :param wh_wv_list: wh wave velocity list [[35, 300]]
    :return: mew combined caldata
    """

    dirpath, fname = os.path.split(combined_cal_data_fpath)

    df = pd.read_csv(combined_cal_data_fpath)

    for ind, wh_wv_arr in enumerate(wh_wv_list):
        df = df[(df['wh'] == wh_wv_arr[0]) & (df['wv'] == wh_wv_arr[1])]
    print('heho')

    df.to_csv(os.path.join(dirpath, 'curate_'+fname), index=False)



if __name__ == '__main__':

    caldata_fpath = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\blend_rad\_subcls\bsa_reversepeptides\Cal_smallmol\output\combined_caldata.csv"
    wh_wv_list = [[30, 900]]
    curate_combined_caldata_wh_wv(caldata_fpath, wh_wv_list)