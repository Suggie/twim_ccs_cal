import numpy as np
import scipy
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors

class MidpointNormalize(mpl.colors.Normalize):
    def __init__(self, vmin, vmax, midpoint=0, clip=False):
        self.midpoint = midpoint
        mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        normalized_min = max(0, 1/2 * (1 - abs((self.midpoint - self.vmin) / (self.midpoint - self.vmax))))
        normalized_max = min(1, 1/2 * (1 + abs((self.vmax - self.midpoint) / (self.midpoint - self.vmin))))
        normalized_mid = 0.5
        x, y = [self.vmin, self.midpoint, self.vmax], [normalized_min, normalized_mid, normalized_max]
        return scipy.ma.masked_array(scipy.interp(value, x, y))


def construct_grid(wh, wv, data):
    wv_unique = np.unique(wv)
    wh_unique = np.unique(wh)
    data_grid = np.zeros(shape=(len(wv_unique), len(wh_unique)))
    ind_new = 0
    for ind, (wv_, wh_) in enumerate(zip(wv, wh)):
        for i, wv_un in enumerate(wv_unique):
            for j, wh_un in enumerate(wh_unique):
                if wv_un == wv_:
                    if wh_un == wh_:
                        data_grid[i, j] = data[ind_new]
                        ind_new += 1

    data_grid[data_grid == 0] = 'nan'
    return data_grid


def plot_imshow(data_grid, x_val, y_val, x_key, y_key, z_key, colormap, color_mode, clim = None):

    if color_mode == 'diverge':
        vmax = np.max(data_grid[~np.isnan(data_grid)])
        vmin = np.min(data_grid[~np.isnan(data_grid)])
        norm = MidpointNormalize(vmin=vmin, vmax=vmax, midpoint=0)
        curr_cmap = plt.cm.get_cmap(colormap)
        curr_cmap.set_bad(color='grey', alpha=1)
        plt.imshow(data_grid, origin='lower', cmap=curr_cmap, clim=(vmin, vmax), norm=norm)
    else:
        curr_cmap = plt.cm.get_cmap(colormap)
        curr_cmap.set_bad(color='grey')
        plt.imshow(data_grid, origin='lower', cmap=curr_cmap)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, len(np.unique(x_val)), 1))
    ax.set_yticks(np.arange(0, len(np.unique(y_val)), 1))
    ax.set_xticklabels(np.unique(x_val))
    ax.set_yticklabels(np.unique(y_val))
    if clim != None:
        plt.clim(clim)
    plt.colorbar()
    plt.xlabel(x_key)
    plt.ylabel(y_key)
    plt.title(z_key)


def plot_imshow_scale(data_grid, x_val, y_val, x_key, y_key, z_key, colormap, vmin, vmax, scale):
    """
    plot imshow with different scales (logarithmic, sym log, power law)
    :param z: z grid
    :param x: x vals
    :param y: y vals
    :param xkey: label x
    :param ykey: label y
    :param zkey: label z
    :param scale: log, sym log, power law
    :param colormap: color map used for plotting
    :param vmin: min value for color
    :param vmax: max value for color
    :return: void
    """

    curr_cmap = plt.cm.get_cmap(colormap)
    curr_cmap.set_bad(color='grey', alpha=1)
    if scale == 'log':
        norm_color = colors.LogNorm(vmin=vmin, vmax=vmax)
    if scale == 'sym_log':
        norm_color = colors.SymLogNorm(linthresh=0.03, linscale=0.03, vmin=vmin, vmax=vmax)
    if scale == 'sqrt':
        norm_color = colors.PowerNorm(gamma=0.5, vmin=vmin, vmax=vmax)
    # if not scale == str:
    #     norm_color = colors.PowerNorm(gamma=scale, vmin=vmin, vmax=vmax)


    plt.imshow(data_grid, origin='lower', cmap=curr_cmap, norm=norm_color)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, len(np.unique(x_val)), 1))
    ax.set_yticks(np.arange(0, len(np.unique(y_val)), 1))
    ax.set_xticklabels(np.unique(x_val))
    ax.set_yticklabels(np.unique(y_val))
    plt.colorbar()
    plt.xlabel(x_key)
    plt.ylabel(y_key)
    plt.title(z_key)



def plot_bar_graph(y_, x_labels, y_label, y_err, color_list=None):
    num = len(y_)
    ind = np.arange(num)
    width = 0.7
    fig, ax = plt.subplots(figsize=(20,10))
    if type(color_list) is list:
        if type(y_err) is list:
            rects = ax.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err,
                           error_kw=dict(lw=1, capsize=1, capthick=1))
        else:
            rects = ax.bar(ind, y_, width, color=color_list, alpha=1)
    else:
        if type(y_err) is list:
            rects = ax.bar(ind, y_, width, color='black', alpha=1)

        else:
            rects = ax.bar(ind, y_, width, color='black', alpha=1, yerr=y_err,
                           error_kw=dict(lw=1, capsize=1, capthick=1))

    plt.xticks(ind, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)


def plot_bar_graph_with_one_breaks_y_axis(y_, y_err, x_labels, y_label, ylim1, ylim2):
    """
    plot bar graph with y breaks [includes all the limits]
    :param y_: y data
    :param y_err: y err data
    :param x_labels: x labels
    :param y_breaks: y breaks
    :return: plot object
    """

    num = len(y_)
    ind = np.arange(num)
    width = 0.7

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(20, 10), gridspec_kw={'height_ratios': [1, 4]})

    if y_err:
        rects1 = ax1.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
        rects2 = ax2.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
    else:
        rects1 = ax1.bar(ind, y_, width, color='black', alpha=1)
        rects2 = ax2.bar(ind, y_, width, color='black', alpha=1)

    ax1.set_ylim(ylim1)
    ax2.set_ylim(ylim2)


    ax1.spines['bottom'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)

    ax1.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)

    ax1.yaxis.set_ticks(ylim1)

    ax2.yaxis.set_ticks(np.arange(ylim2[0], ylim2[1]+1, 1))

    plt.xticks(ind, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)



def plot_bar_graph_with_two_breaks_y_axis(y_, y_err, x_labels, y_label, ylim1, ylim2, ylim3, color_list):
    """
    plot bar graph with y breaks [includes all the limits]
    :param y_: y data
    :param y_err: y err data
    :param x_labels: x labels
    :param y_breaks: y breaks
    :return: plot object
    """

    num = len(y_)
    ind = np.arange(num)
    width = 0.7

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True, figsize=(20, 10), gridspec_kw={'height_ratios': [1, 4, 1]})

    if not color_list:
        if y_err:
            rects1 = ax1.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
            rects2 = ax2.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
            rects3 = ax3.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
        else:
            rects1 = ax1.bar(ind, y_, width, color='black', alpha=1)
            rects2 = ax2.bar(ind, y_, width, color='black', alpha=1)
            rects3 = ax3.bar(ind, y_, width, color='black', alpha=1)

    if color_list:
        if y_err:
            rects1 = ax1.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
            rects2 = ax2.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
            rects3 = ax3.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
        else:
            rects1 = ax1.bar(ind, y_, width, color=color_list, alpha=1)
            rects2 = ax2.bar(ind, y_, width, color=color_list, alpha=1)
            rects3 = ax3.bar(ind, y_, width, color=color_list, alpha=1)

    ax1.set_ylim(ylim1)
    ax2.set_ylim(ylim2)
    ax3.set_ylim(ylim3)

    ax1.spines['bottom'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax3.spines['top'].set_visible(False)

    ax1.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)
    ax3.tick_params(axis='x', which='both', bottom=False, top=False)

    ax1.yaxis.set_ticks(ylim1)
    ax3.yaxis.set_ticks(ylim3)

    ax2.yaxis.set_ticks(np.arange(ylim2[0], ylim2[1]+1, 1))

    plt.xticks(ind, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)



def box_whisker_plot(data, color, marker, xlim=None, ylim=None, y_int=1.0, multiple_data=True):
    """
    plots box whisker plot from the list of 1 d data given
    :param data:
    :return:
    """
    if multiple_data:
        for ind, (data_, color_, marker_) in enumerate(zip(data, color, marker)):
            x_rep = np.random.normal(ind + 1, 0.05, size=len(data_))
            plt.scatter(x_rep, data_, marker=marker_, color=color_, alpha=0.2)
    else:
        x_rep = np.random.normal(1, 0.05, size=len(data))
        plt.scatter(x_rep, data, marker=marker, color=color, alpha=0.2)

    box_prop_dict = dict(linewidth=1.0)
    whisker_prop_dict = dict(linewidth=1.0)
    cap_prop_dict = dict(linewidth=1.0)
    median_prop_dict = dict(linestyle='-', color='black', linewidth=1.0)
    mean_prop_dict = dict(linestyle='-', color='red', linewidth=1.0)
    bp = plt.boxplot(data, showfliers=False, meanline=True, showmeans=True,
                     boxprops=box_prop_dict,
                     whiskerprops=whisker_prop_dict,
                     capprops=cap_prop_dict,
                     medianprops=median_prop_dict,
                     meanprops=mean_prop_dict)
    if xlim:
        plt.xlim(xlim)
    if ylim:
        plt.ylim(ylim)
        plt.yticks(np.arange(np.floor(ylim[0]), np.ceil(ylim[1])+1, y_int))