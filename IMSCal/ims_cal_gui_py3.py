#!/usr/bin/python
##
## IMS Calibration GUI - version 0.1  -  05/08/2019 - DJL
##

import wx, os, subprocess, string, math


# Define File Drop Target class
class FileDropTarget(wx.FileDropTarget):
    def __init__(self, obj):
        # Initialize the wxFileDropTarget Object
        wx.FileDropTarget.__init__(self)
        # Store the Object Reference for dropped files
        self.obj = obj

    def OnDropFiles(self, x, y, filenames):
        # Move Insertion Point to the end of the widget's text
        self.obj.SetInsertionPointEnd()
        # append a list of the file names dropped
        for filename in filenames:
            if len(filename) > 0:
                self.obj.WriteText(filename + '\n')
        return True

    def clearFiles(self):
        self.obj.Clear()


class MainWindow(wx.Frame):
    def __init__(self, parent, id, title):
        super(MainWindow, self).__init__(parent, title=title, size=(800, 600))

        self.SetBackgroundColour(wx.WHITE)

        self.system_settings = {}
        self.default_settings_file_name = 'default_settings.dat'
        nrows = 6
        ncols = 6

        panel = wx.Panel(self)
        sizer = wx.GridBagSizer(nrows, ncols)

        self.text1 = wx.TextCtrl(panel, -1, "", style=wx.TE_READONLY)
        sizer.Add(self.text1, pos=(0, 0), span=(1, ncols), flag=wx.EXPAND | wx.LEFT | wx.TOP, border=15)
        self.text1.SetValue("Ready...")

        cal_label = wx.StaticText(panel, label="Calibration reference file")
        sizer.Add(cal_label, pos=(1, 0), flag=wx.EXPAND | wx.LEFT, border=15)

        self.text2 = wx.TextCtrl(panel, -1, "")
        sizer.Add(self.text2, pos=(2, 0), span=(1, ncols - 1), flag=wx.EXPAND | wx.LEFT, border=15)
        # Create a File Drop Target object
        self.dt2 = FileDropTarget(self.text2)
        # Link the Drop Target Object to the Text Control
        self.text2.SetDropTarget(self.dt2)

        input_label = wx.StaticText(panel, label="Input file list")
        sizer.Add(input_label, pos=(3, 0), flag=wx.EXPAND | wx.LEFT | wx.TOP, border=15)

        self.text3 = wx.TextCtrl(panel, -1, "", style=wx.TE_MULTILINE | wx.HSCROLL)
        sizer.Add(self.text3, pos=(4, 0), span=(nrows - 2, ncols - 1), flag=wx.EXPAND | wx.LEFT, border=15)

        # Make this control a File Drop Target
        # Create a File Drop Target object
        self.dt3 = FileDropTarget(self.text3)
        # Link the Drop Target Object to the Text Control
        self.text3.SetDropTarget(self.dt3)

        self.runbutton = wx.Button(panel, id=wx.ID_ANY, label="Run Calibration")
        self.runbutton.Bind(wx.EVT_BUTTON, self.runCalibration)
        sizer.Add(self.runbutton, pos=(0, ncols), span=(1, 1), flag=wx.ALL, border=15)

        clbutton = wx.Button(panel, id=wx.ID_ANY, label="Clear calibration")
        clbutton.Bind(wx.EVT_BUTTON, self.clearCalibration)
        sizer.Add(clbutton, pos=(2, ncols), flag=wx.ALL, border=15)

        clbutton = wx.Button(panel, id=wx.ID_ANY, label="Clear file list")
        clbutton.Bind(wx.EVT_BUTTON, self.clearFileList)
        sizer.Add(clbutton, pos=(4, ncols), flag=wx.ALL, border=15)

        sizer.AddGrowableRow(4)
        sizer.AddGrowableCol(3)
        panel.SetSizer(sizer)

        # Display the Window
        self.Show(True)

    def CloseWindow(self, event):
        self.Close()

    ## load the file list and run the calibrations
    def runCalibration(self, event):

        self.text1.SetValue("Running...")

        self.runbutton.Disable()

        success_flag = 1

        ## read in default settings
        self.readRefFile(self.default_settings_file_name)

        output_file_list = []

        ref_name = self.text2.GetLineText(0)
        if os.path.isfile(ref_name):

            ## Read system settings from the ref file
            self.readRefFile(ref_name)

            ##  Certain settings need to exist!
            if 'velocity' not in self.system_settings:
                self.text1.SetValue("Failed setting TW velocity")
                success_flag = 0
            if 'voltage' not in self.system_settings:
                self.text1.SetValue("Failed setting TW voltage")
                success_flag = 0
            if 'pressure' not in self.system_settings:
                self.text1.SetValue("Failed setting pressure")
                success_flag = 0
            if 'accuracy' not in self.system_settings:
                self.text1.SetValue("Failed setting accuracy")
                success_flag = 0

            ## run the calibration on the ref file also, this means we can calculate ccs residuals
            input_file_list = [ref_name]
            for i in range(self.text3.GetNumberOfLines()):
                fname = self.text3.GetLineText(i)
                if len(fname) > 0:
                    if os.path.isfile(fname):
                        input_file_list.append(fname)
                    else:
                        print
                        "File %s not found" % (fname)

            for fname in input_file_list:

                output_name = fname

                path, filename = os.path.split(output_name)
                if 'input' in path:
                    path = path.replace('input', 'output')
                if 'input' in filename:
                    filename = filename.replace('input', 'output')
                else:
                    filename = 'output_' + filename

                ## store the output files
                output_name = path + '\\' + filename
                if fname == ref_name:
                    residuals_name = path + '\\' + filename.replace('output', 'residuals')
                output_file_list.append(output_name)

                ## print fname
                ## print output_name

                try:
                    run_list = []
                    run_list.append(r"bin\TWaveCalibrate.exe")
                    run_list.append('-ref')
                    run_list.append(ref_name)  ## this should really be the calibration file!
                    run_list.append('-output')
                    run_list.append(output_name)
                    run_list.append('-input')
                    run_list.append(fname)

                    for key in self.system_settings.keys():
                        run_list.append('-' + key)
                        run_list.append(self.system_settings[key])

                        ##print run_list

                    subprocess.call(run_list)
                except:
                    print("Calibration on file %s failed" % (fname))
                    if fname == ref_name:  ## break if we fail the reference calibration!
                        self.text1.SetValue("Calibration on file %s failed" % (fname))
                        success_flag = 0
                        break

            if success_flag > 0:

                ## calibations all run, we can examine the ref_file calibration for the residuals
                ## we need to load the calib ccs values from the ref_output
                ## and the ref ccs values from the ref file
                frefin = open(ref_name, 'r')
                frefout = open(output_file_list[0], 'r')
                ref_ccs_list = []
                cal_ccs_list = []
                for line in frefin:
                    if '#' not in line:
                        splitline = string.split(line)
                        try:
                            temp = [splitline[0], float(splitline[1]), int(splitline[2]), float(splitline[3])]
                            ref_ccs_list.append(temp)
                        except:
                            pass
                read_fref = False
                for line in frefout:
                    if 'CALIBRATED DATA' in line:
                        read_fref = True
                    if read_fref:
                        if 'Drift' not in line:
                            splitline = string.split(line, ',')
                            try:
                                cal_ccs_list.append(float(splitline[4]))
                            except:
                                pass

                frefin.close()
                frefout.close()

                rmse_ccs = 0.0
                ccs_residuals = []
                if len(cal_ccs_list) != len(ref_ccs_list):
                    print
                    "CCS residual calculation failed, list length mismatch"
                else:
                    ## this step would be faster using numpy, should be ok as is
                    ncal = len(cal_ccs_list)
                    for i in range(ncal):
                        ref_ccs = ref_ccs_list[i][3]
                        ccs_err = 100.0 * (cal_ccs_list[i] - ref_ccs) / ref_ccs
                        ccs_residuals.append(ccs_err)
                        rmse_ccs += ccs_err ** 2
                    rmse_ccs = math.sqrt(rmse_ccs / ncal)

                fresids = open(residuals_name, 'w')
                resid_string = '# RMSE:  %.2f \n' % (rmse_ccs)
                fresids.write(resid_string)
                resid_string = '# ID  mass  z  ref_ccs  cal_ccs  ccs_error \n'
                fresids.write(resid_string)
                for i in range(ncal):
                    id_string = ref_ccs_list[i][0]
                    mass = ref_ccs_list[i][1]
                    charge = ref_ccs_list[i][2]
                    rccs = ref_ccs_list[i][3]
                    resid_string = ' %s %g %d %g %g %g \n' % (
                    id_string, mass, charge, rccs, cal_ccs_list[i], ccs_residuals[i])
                    fresids.write(resid_string)
                fresids.close()

                self.text1.SetValue("Finished! RMSE CCS residuals:  %.2f %% " % (rmse_ccs))

        else:
            ##print "Ref file %s not found" % (ref_name)
            self.text1.SetValue("Ref file %s not found" % (ref_name))

        self.runbutton.Enable()

    def readRefFile(self, ref_file_name):

        settings_list = ['length', 'pressure', 'velocity', 'voltage', 'lambda', 'accuracy', 't0', 'a', 'c']

        f = open(ref_file_name, 'r')
        for line in f:
            if '#' in line:
                line = line.replace('#', '')
                line = line.replace('-', '')
                # splitline = string.split(line)
                splitline = line.split()
                for setting in settings_list:
                    if setting == splitline[0]:
                        self.system_settings[setting] = splitline[-1]

        f.close()

    def clearFileList(self, event):
        self.dt3.clearFiles()

    def clearCalibration(self, event):
        self.dt2.clearFiles()


class MyApp(wx.App):
    def OnInit(self):
        # Declare the Main Application Window
        frame = MainWindow(None, -1, "TW-IMS Calibration tool")
        # Show the Application as the top window
        self.SetTopWindow(frame)
        return True


# Declare the Application and start the Main Loop
if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()
