
GUI info:

Requires python 2.7 and wxpython 

Reads settings from default_settings.dat

For the calibration we need to supply a reference file, ie drift times for ions with known (reference) ccs values.
Reference and input files share the same format:
ID mass z ccs(Ang^2) drift time(ms)
For the input files the ccs column can be present or absent, if present it is ignored. 
In the header of the reference file we supply the remaining settings, eg TW voltage/velocity. Reference file settings overwrite any default settings.

Drag and drop (or paste paths) for reference and input files. We can have multiple input files for a single reference file, these should all share the same settings (eg TW velocity/voltage, pressure)

The "Run Calibration" button runs the calibration. Output files match input files with "output" replaced by input in the path and filename, or "output_" prepended if output is not present in the filename.
We also run the calibration on the reference file, again the output of this has "output_" prepended. CCS residuals are calculated based on the reference file, the RMSE of these is reported in the status window.
The full residuals are written to the reference file name with "residuals_" prepended. The format of this file is:
ID  mass  z  reference_ccs  calibrated_ccs  ccs_error(%)



Calibration method:

Details of the calibration function are given in the ASMS 2019 poster (we use eqn 7 + eqn 8 for the radial term).
We fit g,a,c and t0

Settings that are required:
velocity : Twave velocity (m/s)
voltage  : Twave voltage (V)
pressure : IMS cell pressure (mbar)
accuracy : estimated uncertainty in the drift time measurement (%)

Optional settings:
temp     : cell temperature (K), defaults to 300
length   : cell length (m), defaults to 0.254
lambda   : wavelength (m), defaults to 0.012
t0       : fixed time offset (s), this fixes t0 in the calibration to the specified value.
a        : fixed vel. relaxation parameter, this fixes a in the calibration to the specified value.
c        : fixed radial parameter, this fixes c in the calibration to the specified value.



Output file format:

[Parameters]
Mean and standard deviation values for the fitting parameters g,a,c,t0
[Diagnostics]
Outputs reference ion information (drift velocity in m/s)  
ID,Mass,Z,Mobility,Alpha,Gamma,Model Vel.,Exp. Vel.,Error %

[CALIBRATED DATA]
Outputs information for the calibrated ions, calibrated ccs and std.dev. ccs.
ID,Mass,Z,Drift,CCS,CCS Std.Dev.





