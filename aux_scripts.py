import os
import numpy as np
import pandas as pd
import scipy
import matplotlib as mpl
from math import sqrt
from scipy.stats import iqr, ttest_ind, sem, t
# import matplotlib.pyplot as plt


def correct_time_for_tof(time, ion_mass, ion_charge, edc_constant):
    """
    correct drift time for mass dependent flight time
    :param time: drift time
    :param ion_mass: ion mass
    :param ion_charge: ion charge
    :param edc_constant: edc constant used
    :return:corrected drift time
    """
    t_corr = time - (edc_constant*sqrt(ion_mass/ion_charge)*(1/1000))
    return t_corr


def get_data_stats(data, confidence=0.95):
    """
    Calculate the confidence interval given the data
    :param data:
    :param confidence:
    :return: mean, margin_error, ci_low, ci_high
    """
    stat_dict = dict()
    stat_dict['num'] = len(data)
    stat_dict['mean'] = np.mean(data)
    stat_dict['median'] = np.median(data)
    stat_dict['std_dev'] = np.std(data)
    stat_dict['std_err'] = sem(data)
    stat_dict['confidence_int'] = stat_dict['std_err'] * t.ppf((1 + confidence) / 2, stat_dict['num'] - 1)
    stat_dict['confidence_int_low'] = stat_dict['mean'] - stat_dict['confidence_int']
    stat_dict['confidence_int_high'] = stat_dict['mean'] + stat_dict['confidence_int']
    stat_dict['low_q'], stat_dict['high_q'] = np.quantile(data, [.25, .75])
    stat_dict['iqr'] = iqr(data)
    stat_dict['low_whisker'] = stat_dict['low_q'] - 1.5 * stat_dict['iqr']
    stat_dict['high_whisker'] = stat_dict['high_q'] + 1.5 * stat_dict['iqr']
    stat_dict['5_percentile'] = np.percentile(data, 5)
    stat_dict['95_percentile'] = np.percentile(data, 95)

    return stat_dict



class MidpointNormalize(mpl.colors.Normalize):
    def __init__(self, vmin, vmax, midpoint=0, clip=False):
        self.midpoint = midpoint
        mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        normalized_min = max(0, 1/2 * (1 - abs((self.midpoint - self.vmin) / (self.midpoint - self.vmax))))
        normalized_max = min(1, 1/2 * (1 + abs((self.vmax - self.midpoint) / (self.midpoint - self.vmin))))
        normalized_mid = 0.5
        x, y = [self.vmin, self.midpoint, self.vmax], [normalized_min, normalized_mid, normalized_max]
        return scipy.ma.masked_array(scipy.interp(value, x, y))


def rms_error(ref_ccs, pred_ccs):
    """
    calculate rmse between reference and predicted ccs values
    :param ref_ccs: reference ccs values array
    :param pred_ccs: predicted ccs values array
    :return: rms error
    """
    diff = np.subtract(pred_ccs, ref_ccs)
    percent_error_sq = np.square(np.multiply(np.divide(diff, ref_ccs), 100))
    rmse = np.sqrt(np.divide(np.sum(percent_error_sq), len(percent_error_sq)))
    return rmse

def diff_avg_std_for_arr(ref_ccs_arr, pred_ccs_arr):
    """
    calculate the average std deviation between two arrays
    :param ref_ccs_arr:
    :param pred_ccs_arr:
    :return:
    """
    diff_percent = np.divide(np.multiply(np.subtract(pred_ccs_arr, ref_ccs_arr), 100), ref_ccs_arr)
    diff_average = np.average(diff_percent)
    diff_std = np.std(diff_percent)
    return diff_average, diff_std


def get_files(dirpath, endid, startid=None):
    """
    get a list of all ref files
    :param dirpath: dirpath
    :param endid: ending id
    :return: list of files
    """
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    if startid:
        files = [x for x in files if x.startswith(startid)]
    return files


def gen_wh_wv_key_(wh, wv):
    """
    generate str of concatenated wh and wv values
    :param wh: wave height value
    :param wv: wave velocity value
    :return: key str
    """
    key_ = str(wh)+'_'+str(wv)
    return key_


def gen_a_linear_model_wv(input_wv, bounds_values=[0.89, 1.17], bounds_wv=[300, 1000]):
    """
    gen a list of wv and fix a values given the bounds for both parameter and wv and using a linear model
    :param bounds_values: [low, high]
    :param bounds_wv: [low, high]
    :return: list
    """
    slope = (bounds_values[1] - bounds_values[0])/(bounds_wv[1] - bounds_wv[0])
    intercept = bounds_values[0] - slope*bounds_wv[0]
    fix_val = round(slope*input_wv + intercept, 2)
    return fix_val


def create_dirpath(dirpath):
    """
    creates a dirpath is the dirpath doesn't exist
    :param dirpath: dirpath
    :return: return dirpath
    """
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    return dirpath


def get_cal_info_dict_from_gauss_flist(gauss_file_list_fpath, gas_type='n2', ref_ions=True):
    """
    gen dict with info from the gauss flie list and store gauss dfs to a list
    :param gauss_file_list_fpath: file path for gauss file list for cal
    :param ccs_database_df: ccs database pandas df
    :param temperature: temperature
    :return: dict
    """
    ccs_database_df = pd.read_csv(os.path.join(os.getcwd(), "CCSDatabase\ccsdatabse_positive.csv"))

    gauss_file_list_df = pd.read_csv(gauss_file_list_fpath)
    species_id = gauss_file_list_df['species_name'].values
    oligomer = gauss_file_list_df['oligomer'].values
    mass = gauss_file_list_df['mass'].values
    charge = gauss_file_list_df['charge'].values
    gauss_fpath_list = gauss_file_list_df['gauss_fpath'].values
    if not ref_ions:
        ccs_ = len(species_id)*[0]
    else:
        ccs_ = []
        for index, (species, oligo, charge_) in enumerate(zip(species_id, oligomer, charge)):
            df_select = ccs_database_df[
                (ccs_database_df['id'] == species) & (ccs_database_df['n_oligomers'] == oligo) & (
                            ccs_database_df['z'] == charge_)]
            ccs = df_select['ccs_'+gas_type].values[0] * 100
            ccs_.append(ccs)
    cal_dict = dict()
    cal_dict['species'] = species_id
    cal_dict['oligomer'] = oligomer
    cal_dict['mass'] = mass
    cal_dict['charge'] = charge
    cal_dict['gauss_fpath_list'] = gauss_fpath_list
    cal_dict['ccs'] = ccs_

    return cal_dict


def string_to_batch_file(cal_executable_path, ref_file, cal_input_file, output_file, wave_height, wave_velocity,
                        length=0.254, pressure=3.4, temperature=300, lambda_=0.012, accuracy=3, fix_params=None):
    """
    write the string to batch file
    :param cal_executable_path: executable file path for ccs cal
    :param ref_file: reference input file path
    :param cal_input_file: cal input file path
    :param output_file: output file path
    :param wave_height: wave height
    :param wave_velocity: wave velocity
    :param length: length of cell
    :param pressure: pressure
    :param temperature: temperature
    :param lambda_: wave length
    :param t_0: fixed time off set
    :param vel_relax_param: fixed velocity relaxation parameter
    :param radial_param: fixed radial parameter
    :return: string
    """

    batch_string = ''
    line = '{} -ref "{}" -input "{}" -output "{}" -voltage {} -velocity {} -length {} -pressure {} -temp {} -lambda {} -accuracy {}'.format(
        cal_executable_path,
        ref_file,
        cal_input_file,
        output_file,
        wave_height,
        wave_velocity,
        length,
        pressure,
        temperature,
        lambda_,
        accuracy)

    if fix_params:
        fix_par_string = ''
        for index, arr in enumerate(fix_params):
            if arr[0] == 'scale_a':
                val = gen_a_linear_model_wv(input_wv=wave_velocity)
                fix_str = ' -a ' + str(val)
                fix_par_string += fix_str
            else:
                fix_str = ' -' + arr[0] + ' ' + str(arr[1])
                fix_par_string += fix_str

        line += fix_par_string

    batch_string += line + '\n'

    return batch_string


def get_a_scale_values_from_wv_bounds(wv_bounds=[300,1000], wv_incr=100):
    """
    get a values using a scale function
    :param wv_bounds: bounds for wave velocities
    :param wv_incr: increament for wave velocities
    :return: a value list
    """

    wv_range = np.arange(wv_bounds[0], wv_bounds[1], wv_incr)
    a_list = []
    for wv in wv_range:
        a_ = gen_a_linear_model_wv(wv)
        print(wv, a_)