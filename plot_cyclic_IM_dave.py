import os
import pandas as pd
import matplotlib.pyplot as plt

def get_files(dirpath, endid, startid=None):
    """
    get a list of all ref files
    :param dirpath: dirpath
    :param endid: ending id
    :return: list of files
    """
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    if startid:
        files = [x for x in files if x.startswith(startid)]
    return files

def plot_bar_graph(y_, x_labels, y_label, y_err, color_list=None):
    num = len(y_)
    ind = np.arange(num)
    width = 0.7
    fig, ax = plt.subplots(figsize=(20,10))
    if type(color_list) is list:
        if type(y_err) is list:
            rects = ax.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err,
                           error_kw=dict(lw=1, capsize=1, capthick=1))
        else:
            rects = ax.bar(ind, y_, width, color=color_list, alpha=1)
    else:
        if type(y_err) is list:
            rects = ax.bar(ind, y_, width, color='black', alpha=1)

        else:
            rects = ax.bar(ind, y_, width, color='black', alpha=1, yerr=y_err,
                           error_kw=dict(lw=1, capsize=1, capthick=1))

    plt.xticks(ind, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)

def plot_cyclic_data(dirpath, file_end_id, color_code_file):
    files = get_files(dirpath, endid=file_end_id)

    for ind, file in enumerate(files):
        df = pd.read_csv(os.path.join(dirpath, file), sep='\s+', header=None)
        y_ = df.iloc[:, 2].values
        y_err_list = list(df.iloc[:, 3].values)
        x_labels = df.iloc[:, 0].values
        color_df = pd.read_csv(color_code_file)
        color_list = list(color_df['hex_code'].values)

        plot_bar_graph(y_=y_,
                       y_err=y_err_list,
                       x_labels=x_labels,
                       y_label='ccs_dev',
                       color_list=color_list)

        plt.savefig(os.path.join(dirpath, file + 'ccsdev.pdf'))
        plt.close()

if __name__=='__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\__cyclic_IM_data\cyclic_calibration_species_noncal"
    color_file = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\__cyclic_IM_data\color_code.csv"
    plot_cyclic_data(dirpath, '.dat', color_file)