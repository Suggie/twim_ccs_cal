# perform power law calibrations based on the TwaveCal_batchfile
import os
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit


def percent_error(pred_ccs, ref_ccs):
    """
    calculate percent error
    :param pred_ccs: pred ccs error
    :param ref_ccs: ref ccs error
    :return: error deviation values
    """
    error_deviation = 100*(pred_ccs - ref_ccs)/ref_ccs
    return error_deviation


def corr_ccs_to_ccs(corr_ccs, charge, reduced_mass):
    """
    Calculate CCS from corrected ccs
    :param corr_ccs: corrected ccs
    :param charge: charge
    :param reduced_mass: reduced mass
    :return: CCS (un corrected)
    """
    y = corr_ccs * (charge * np.sqrt(1/reduced_mass))
    return y


def mass_da_to_kg(mass):
    """
    convert mass in daltons to kg
    :param mass: mass in da
    :return: mass in kg
    """
    return mass*1.66054e-27


def reduced_mass(mass1, mass2):
    """
    compute reduce mass of two bodies
    :param mass1:
    :param mass2:
    :return: reduce mass
    """
    y = (mass1*mass2)/(mass1 + mass2)
    return y


def correct_ccs(ccs, charge, reduce_mass):
    """
    calculate the corrected ccs based on charge and reduce mass
    :param ccs: ccs
    :param charge: charge state
    :param reduce_mass: reduce mass
    :return: corrected ccs
    """
    y = ccs/(charge * np.sqrt(1/reduce_mass))
    return y


def power_law_fit(X, a, b, t0):
    """
    power law function with t0 variable
    :param X: corr_time, z
    :param a: fitting param
    :param b: fitting param
    :param t0: fitting param
    :return: corr_ccs
    """
    corr_time, z = X
    corr_ccs = a*(corr_time - t0)**b
    return corr_ccs


def power_law_radial_fit(X, a, b, t0, cexp):
    """
    power law + radial function with t0 variable
    :param X: corr_time, z
    :param a: fitting param
    :param b: fitting param
    :param t0: fitting param
    :return: corr_ccs
    """
    corr_time, z = X
    corr_ccs = (a*(corr_time - t0)**b) * np.exp(cexp/(z**0.5))
    return corr_ccs


def rms_error(predict_values, true_values):
    """
    Calculate the percent error and computes the rms error in %
    :param predict_values: predict values
    :param true_values: true values
    :return: % rmse
    """
    diff = np.subtract(predict_values, true_values)
    percent_error = np.multiply(np.divide(diff, true_values), 100)
    percent_error_sq = np.square(percent_error)
    rmse = np.sqrt(np.divide(np.sum(percent_error_sq), len(percent_error_sq)))
    return rmse


def read_input_output_files(fpath, gas='n2'):
    """
    read input and output files
    :param fpath: file path
    :return: df
    """
    df = pd.read_csv(fpath, delim_whitespace=True, header=None)
    df.columns = ['id', 'mass', 'charge', 'ccs', 'corr_time']
    gas_mass = 0
    if gas == 'n2':
        gas_mass += 28
    if gas == 'he':
        gas_mass += 4
    reduce_mass = reduced_mass(mass_da_to_kg(df.iloc[:,1].values), mass_da_to_kg(gas_mass))
    corr_ccs = correct_ccs(df['ccs'].values, df['charge'].values, reduce_mass)
    df['reduce_mass'] = reduce_mass
    df['corr_ccs'] = corr_ccs
    return df


def write_output(ref_df, unk_df, popt, pred_ref_ccs, pred_unk_ccs, output_fpath):
    """
    write the output to a file
    :param ref_df: reference df
    :param unk_df: unknown df
    :param popt: optimized paramters
    :param pred_ref_ccs: pred_ref_ccs array
    :param pred_unk_ccs: pred_unk_ccs array
    :param output_fpath: output file path
    :return:None
    """
    pred_ref_ccs_deviation = percent_error(pred_ref_ccs, ref_df['ccs'].values)

    rmse_percent = rms_error(pred_ref_ccs, ref_df['ccs'].values)
    pred_unk_ccs_dev = (rmse_percent/100)*pred_unk_ccs

    output_string = ''
    param_header = '[PARAMETERS]\n'+'a,b,t0,cexp\n'
    param_string = ','.join([str(x) for x in popt])+'\n'

    diag_header = '[DIAGNOSTICS]\n'+'ID,MASS,Z,CCS,PRED_CCS,ERROR%\n'
    diag_string = ''
    for ind, (ref_id, ref_mass, ref_charge, ref_ccs, ref_pred_ccs, ref_ccs_error) in enumerate(zip(ref_df['id'].values,
                                                                                                   ref_df['mass'].values,
                                                                                                   ref_df['charge'].values,
                                                                                                   ref_df['ccs'].values,
                                                                                                   pred_ref_ccs,
                                                                                                   pred_ref_ccs_deviation)):
        line = '{},{},{},{},{},{}\n'.format(ref_id, ref_mass, ref_charge, ref_ccs, ref_pred_ccs, ref_ccs_error)
        diag_string += line


    calib_header = '[CALIBRATED DATA]\n'+'ID,MASS,Z,Drift,CCS,CCS Std.Dev.\n'
    calib_string = ''
    for ind, (unk_id, unk_mass, unk_charge, unk_drift, unk_pred_ccs, unk_ccs_dev) in enumerate(zip(unk_df['id'].values,
                                                                                                   unk_df['mass'].values,
                                                                                                   unk_df['charge'].values,
                                                                                                   unk_df['corr_time'],
                                                                                                   pred_unk_ccs,
                                                                                                   pred_unk_ccs_dev)):
        line = '{},{},{},{},{},{}\n'.format(unk_id, unk_mass, unk_charge, unk_drift, unk_pred_ccs, unk_ccs_dev)
        calib_string += line

    output_string = param_header+param_string+'\n'+diag_header+diag_string+'\n'+calib_header+calib_string

    with open(output_fpath, 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


def power_law_cal(ref_fpath, unk_fpath, output_fpath, radial=False):
    """
    perform calibration
    :param ref_df: reference df
    :param unk_df: unknown df
    :param radial: radial bool
    :return:
    """
    ref_df = read_input_output_files(ref_fpath)
    unk_df = read_input_output_files(unk_fpath)

    x_ = (ref_df['corr_time'].values, ref_df['charge'].values)
    y_ = ref_df['corr_ccs'].values
    x_unk = (unk_df['corr_time'].values, unk_df['charge'].values)
    if radial:
        popt, pcov = curve_fit(power_law_radial_fit, x_, y_, maxfev=10000, gtol=1e-20)
        pred_ref_corr_ccs = power_law_radial_fit(x_, *popt)
        pred_unk_corr_ccs = power_law_radial_fit(x_unk, *popt)
    else:
        popt, pcov = curve_fit(power_law_fit, x_, y_, maxfev=10000, gtol=1e-20)
        pred_ref_corr_ccs = power_law_fit(x_, *popt)
        pred_unk_corr_ccs = power_law_fit(x_unk, *popt)

    pred_ref_ccs = corr_ccs_to_ccs(pred_ref_corr_ccs, ref_df['charge'].values, ref_df['reduce_mass'].values)
    pred_unk_ccs = corr_ccs_to_ccs(pred_unk_corr_ccs, unk_df['charge'].values, unk_df['reduce_mass'].values)

    write_output(ref_df, unk_df, popt, pred_ref_ccs, pred_unk_ccs, output_fpath)


def run_cal_from_batch_file(batch_file_path):
    """
    run calibration from a batch file
    :return:
    """

    with open(batch_file_path, 'r') as batch_file:
        batch_file_read = batch_file.read().splitlines()
        for ind, batch_line in enumerate(batch_file_read):
            chars = batch_line.split('"')
            ref_fpath = chars[1]
            unk_fpath = chars[3]
            output_fpath = chars[5]
            index = batch_line.find('-radial')
            if batch_line.find('-radial') != -1:
                power_law_cal(ref_fpath, unk_fpath, output_fpath, radial=True)
            else:
                power_law_cal(ref_fpath, unk_fpath, output_fpath, radial=False)


if __name__ == '__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\rad\_subclass\all"
    powerlaw_batch_file = os.path.join(dirpath, 'PowerLaw_batchfile.txt')
    run_cal_from_batch_file(powerlaw_batch_file)