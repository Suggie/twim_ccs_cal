import os
from ref_unk_calibration_module import write_input_files_for_ref_and_unk as run_cal_ref_unk
from leave_one_ion_cross_val_calibration_module import make_input_files_and_run_calibration as run_cal_leaveoneion
from leave_one_species_cross_val_calibration_module import make_input_files_and_run_calibration as run_cal_leaveonespec

#set up working directory

working_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\_norad\scale_a\peptides"


gauss_flist_ref_fpath = os.path.join(working_dir, 'gauss_file_list_for_cal.csv')
all_dirpath = os.path.join(working_dir, 'all')
all_ref_input_dirpath = os.path.join(all_dirpath, 'ref_input')
leave_one_ion_dpath = os.path.join(working_dir, 'leaveoneion')
leave_one_spec_dpath = os.path.join(working_dir, 'leaveonespec')

cal_exec_file = os.path.join(os.getcwd(), r"IMSCal\bin\TWaveCalibrate")
cal_batch_name = 'TwaveCal_batchfile.bat'

# fix params selections
fix_a_ = [['a', 1]]
fix_c_ = [['c', 0]]
fix_a_c_ = [['a', 1], ['c', 0]]
fix_c_scale_a = [['c', 0], ['scale_a']]
scale_a = [['scale_a']]

# some settings
pressure = 3.4
temperature = 298
accuracy = 3

# run ref calibration
run_cal_ref_unk(all_dirpath, gauss_flist_ref_fpath, gauss_flist_ref_fpath, cal_exec_file, cal_batch_name,
                pressure=pressure, temperature=temperature, accuracy=accuracy, fix_params=fix_c_scale_a, run_cal=True)

# run leave one ion calibration
run_cal_leaveoneion(all_ref_input_dirpath, leave_one_ion_dpath, cal_exec_file, cal_batch_name, pressure=pressure,
                    temperature=temperature, accuracy=accuracy, fix_params=fix_c_scale_a, run_cal=True)

# run leave one species calibration
run_cal_leaveonespec(all_ref_input_dirpath, leave_one_spec_dpath, cal_exec_file, cal_batch_name, pressure=pressure,
                     temperature=temperature, accuracy=accuracy, fix_params=fix_c_scale_a, run_cal=True)