# create modules for doing single / multi calibrations

import pandas as pd
import os
import subprocess
import time
from aux_scripts import string_to_batch_file, get_cal_info_dict_from_gauss_flist, create_dirpath

def write_input_files_and_batch_file_for_ref(dirpath, cal_executable_fpath, ref_cal_dict, ref_cal_name, unk_cal_name, batch_fname,
                      temperature=300, pressure=3.4, accuracy=3, lambda_value=0.012, length_cell=0.254,
                      fix_params_list=None):
    """
    write ref/input file to dirpath given the cal dict
    writes for all wave height and wave velocity conditions
    :param cal_dict: cal dict from get_cal_info_dict
    :param dirpath:directory to save the file
    :param output_label:
    :return:
    """

    # First create directories for ref input and unk input files
    # Create directories for ref cal output and unk cal output files

    input_dirpath = create_dirpath(os.path.join(dirpath, ref_cal_name + '_input'))
    # input_unk_dirpath = create_dirpath(os.path.join(dirpath, unk_cal_name + '_input'))
    output_dir = create_dirpath(os.path.join(dirpath, unk_cal_name + '_output'))

    ###
    # check the wave height wave velocities in the the gauss path list and make input file for each wave ht & vel.
    # conditions

    gauss_df_0 = pd.read_csv(ref_cal_dict['gauss_fpath_list'][0])
    wh_conditions, wv_conditions = gauss_df_0.iloc[:, 0].values, gauss_df_0.iloc[:, 1].values

    cal_dict_dir_list = [ref_cal_name, ref_cal_dict, input_dirpath]

    batch_string_ = ''

    for ind, (wv_ht, wv_vel) in enumerate(zip(wh_conditions, wv_conditions)):

        label_name = cal_dict_dir_list[0]
        cal_dict = cal_dict_dir_list[1]
        outdir = cal_dict_dir_list[2]

        input_string = ''

        for index in range(len(cal_dict['species'])):
            gauss_df = pd.read_csv(cal_dict['gauss_fpath_list'][index])
            drift_time_ = gauss_df[(gauss_df.iloc[:, 0] == wv_ht) & (gauss_df.iloc[:, 1] == wv_vel)].iloc[:, 4].values[0]
            id_ = '_'.join([str(cal_dict['species'][index]), str(cal_dict['oligomer'][index])])
            line = '{} {} {} {} {}\n'.format(id_,
                                              cal_dict['mass'][index],
                                              cal_dict['charge'][index],
                                                 cal_dict['ccs'][index],
                                              drift_time_)
            input_string += line

        input_fname = label_name + '_wh_' + str(wv_ht) + '_wv_' + str(wv_vel) + '.dat'
        input_fpath = os.path.join(outdir, input_fname)

        with open(input_fpath, 'w') as input_file:
            input_file.write(input_string)
            input_file.close()

        cal_output_fpath = os.path.join(output_dir, label_name + '_wh_' + str(wv_ht) + '_wv_' + str(wv_vel) + '_output.csv')

        batch_line = string_to_batch_file(cal_executable_fpath, input_fpath, input_fpath,
                                          cal_output_fpath, wv_ht, wv_vel, length_cell, pressure, temperature,
                                          lambda_value, accuracy, fix_params_list)
        batch_string_ += batch_line

    with open(os.path.join(dirpath, batch_fname), 'w') as batch_file:
        batch_file.write(batch_string_)
        batch_file.close()


def ref_file_calibration(gauss_file_list, output_dir, cal_exec_fpath, batch_file_name, fix_params_list=None, run_cal=True):
    """
    make a module for ref file calibration so it can be used for residual analysis
    :param cal_dict: cal dict
    :param output_dir: output directory
    :param cal_exec_fpath: cal executable file path
    :param fix_params_list: fix params list [['a', 1]]
    :return: none
    """

    print('Start assembling ...')

    start_time = time.time()

    cal_dict = get_cal_info_dict_from_gauss_flist(gauss_file_list, ref_ions=True)

    print('Writing files ...')

    write_input_files_and_batch_file_for_ref(output_dir, cal_exec_fpath, cal_dict, 'ref', 'ref_unk', batch_file_name,
                      fix_params_list=fix_params_list)

    print('Made all files...')

    if run_cal:
        print('Now running calibration ... ')
        subprocess.run(os.path.join(output_dir, batch_file_name))

    print('\nFinished! It took blimey ', round((time.time()-start_time)/60, 2), ' minutes')


if __name__ == '__main__':

    gauss_file_list_ = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_subcls\bsa_polya789\ref_gauss_file_list_for_cal.csv"
    test_dir = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\vary_a\_subcls\bsa_polya789\cal_bsa_avd_polya"
    cal_exec_file = os.path.join(os.getcwd(), r"IMSCal\bin\TWaveCalibrate")
    batch_file_name = 'TwaveCal_batchfile.bat'
    fix_a_ = [['a', 1]]
    scale_a = ['a_func']
    ref_file_calibration(gauss_file_list_, test_dir, cal_exec_file, batch_file_name, fix_params_list=None,
                         run_cal=True)