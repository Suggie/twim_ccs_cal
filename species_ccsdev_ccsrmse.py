import os
import numpy as np
import pandas as pd
from aux_scripts import rms_error, diff_avg_std_for_arr, get_data_stats


def gen_ccs_dev_array_stats(caldata_fpath, wh_wv_list=None):
    """
    generate stats for ccs dev array
    :param caldata_fpath: caldata fpath
    :return: None. Save file with information
    """
    dirpath, fname = os.path.split(caldata_fpath)

    df = pd.read_csv(caldata_fpath)



    # get stats on mean, median, interquartile range on ccs deviation
    ref_ccs = df['ref_ccs'].values
    pred_ccs = df['ccs'].values
    ccs_dev_array = np.divide(np.multiply(np.subtract(pred_ccs, ref_ccs), 100), ref_ccs)
    abs_ccs_dev_array = np.divide(np.multiply(np.abs(np.subtract(pred_ccs, ref_ccs)), 100), ref_ccs)
    stat_dict_ccs_dev_array = get_data_stats(ccs_dev_array)
    stat_dict_abs_ccs_dev_array = get_data_stats(abs_ccs_dev_array)
    stat_dict_list = [stat_dict_ccs_dev_array, stat_dict_abs_ccs_dev_array]
    stat_dict_label = ['ccs_dev_array', 'abs_ccs_dev_array']

    data_string = ''

    header = 'label,num,mean,median,std_dev,q1,q3,iqr,low_whisk,high_whisk,std_err,ci_low,ci_high,5th_percent,95th_percent\n'
    for index, (stat_dict, label) in enumerate(zip(stat_dict_list, stat_dict_label)):
        data_string += '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(label, stat_dict['num'],
                                                                                  stat_dict['mean'],
                                                                                  stat_dict['median'],
                                                                                  stat_dict['std_dev'],
                                                                                  stat_dict['low_q'],
                                                                                  stat_dict['high_q'],
                                                                                  stat_dict['iqr'],
                                                                                  stat_dict['low_whisker'],
                                                                                  stat_dict['high_whisker'],
                                                                                  stat_dict['std_err'],
                                                                                  stat_dict['confidence_int_low'],
                                                                               stat_dict['confidence_int_high'],
                                                                               stat_dict['5_percentile'],
                                                                               stat_dict['95_percentile'])

    with open(os.path.join(dirpath, 'caldata_ccs_dev_stats_'+fname), 'w') as outfile:
        outfile.write(header+data_string)
        outfile.close()


def gen_species_ccs_dev_rmse(caldata_fpath):
    """
    generate ccs dev and rmse for each unique species
    :param caldata_fpath: caldata fpath
    :return:
    """

    dirpath, fname = os.path.split(caldata_fpath)

    df = pd.read_csv(caldata_fpath)

    df['spec_id'] = df['id'] + '_' + df['oligo_num'].map(str) + '_' + df['z'].map(str)

    unique_spec_id = np.unique(df['spec_id'].values)

    ccs_dev_avg_arr = []
    ccs_dev_std_arr = []
    ccs_rmse_arr = []
    mass_arr = []

    for index, uniq_id in enumerate(unique_spec_id):
        df_uniq = df[df['spec_id'] == uniq_id]
        mass_arr.append(df_uniq['mass'].values[0])
        ccs_dev_avg, ccs_dev_std = diff_avg_std_for_arr(df_uniq['ref_ccs'].values, df_uniq['ccs'].values)
        ccs_rmse = rms_error(df_uniq['ref_ccs'].values, df_uniq['ccs'].values)
        ccs_dev_avg_arr.append(ccs_dev_avg)
        ccs_dev_std_arr.append(ccs_dev_std)
        ccs_rmse_arr.append(ccs_rmse)

    data_string = ''

    for num in range(len(unique_spec_id)):
        try:
            id_, oligo_num, charge = unique_spec_id[num].split('_')
        except ValueError:
            spec_chars = unique_spec_id[num].split('_')
            id_ = '_'.join(x for x in spec_chars[:2])
            oligo_num = spec_chars[2]
            charge = spec_chars[3]

        data_string += '{},{},{},{},{},{},{}\n'.format(id_, oligo_num, mass_arr[num], charge, str(ccs_rmse_arr[num]),
                                                    str(ccs_dev_avg_arr[num]), str(ccs_dev_std_arr[num]))

    header = 'id,oligo_num,mass,charge,ccs_rmse,ccs_dev_avg,ccs_dev_std\n'

    with open(os.path.join(dirpath, 'caldata_species_ccs_rmse_dev_'+fname), 'w') as outfile:
        outfile.write(header+data_string)
        outfile.close()

    print('heho')


if __name__ == '__main__':
    caldata = r"C:\Users\sugyan\Documents\Processed data\081419_CalProcessing\scale_a\_subcls\bsa_reversepeptides\boxwhisk\8.csv"
    gen_species_ccs_dev_rmse(caldata)
    gen_ccs_dev_array_stats(caldata)