import os
import pandas as pd
import numpy as np
import subprocess
import time
from aux_scripts import create_dirpath, string_to_batch_file, get_files


def get_unique_species_list(dirpath, ref_input_file):
    df = pd.read_csv(os.path.join(dirpath, ref_input_file), delim_whitespace=True, header=None)
    unique_species = np.unique(df.iloc[:, 0].values)
    unique_spec_ = []
    for spec in unique_species:
        if spec.startswith('PolyAla'):
            unique_spec_.append('PolyAla')
        else:
            unique_spec_.append(spec)
    unique_spec_ = np.unique(np.array(unique_spec_))
    return unique_spec_


def make_input_files_and_run_calibration(all_ion_dirpath, outdir, cal_exec_file, cal_batch_fname, pressure,
                                         temperature,
                                         accuracy,
                                         length=0.254, lambda_=0.012,
                                         fix_params=None, run_cal=True):
    """
    make input files for leave one species out cross validation and run the calibration
    :param all_ion_dirpath: all ion dirpath for input files
    :param outdir: directory to save output files
    :param cal_exec_file: IMSCal executable file path
    :param cal_batch_fname: batch file name '.bat'
    :param pressure: pressure in 3.4 mbar
    :param temperature: temp in K
    :param accuracy: %
    :param length: length of cell
    :param lambda_: wave length
    :param fix_params: fix params
    :param run_cal: if true, run the .bat file
    :return: None
    """

    print('Making input files ... ')

    ref_file_list = get_files(all_ion_dirpath, endid='.dat')

    batch_string = ''

    ref_dirpath = create_dirpath(os.path.join(outdir, 'ref_input'))
    unk_dirpath = create_dirpath(os.path.join(outdir, 'unk_input'))
    output_dirpath = create_dirpath(os.path.join(outdir, 'output'))

    unique_species = get_unique_species_list(all_ion_dirpath, ref_file_list[0])

    for ind, uniq_spec in enumerate(unique_species):

        for index, file in enumerate(ref_file_list):

            unk_id = ''

            file_str = str(file).split('.dat')[0]
            file_chars = file_str.split('_')
            wh = float(file_chars[2])
            wv = float(file_chars[4])

            with open(os.path.join(all_ion_dirpath, file), 'r') as all_ion_file:

                all_ion_file_read = all_ion_file.read().splitlines()

                ref_file_string = ''
                unk_file_string = ''

                if not uniq_spec.startswith('PolyAla'):

                    unk_id += uniq_spec

                    # ref_file_string = ''
                    # unk_file_string = ''

                    for num in range(len(all_ion_file_read)):
                        if all_ion_file_read[num].startswith(uniq_spec):
                            unk_file_string += all_ion_file_read[num] + '\n'
                        else:
                            ref_file_string += all_ion_file_read[num] + '\n'

                    ## write file at this tab point

                    ref_fpath = os.path.join(ref_dirpath,
                                             'ref_wh_' + str(wh) + '_wv_' + str(wv) + '#' + unk_id + '.dat')
                    unk_fpath = os.path.join(unk_dirpath,
                                             'unk_wh_' + str(wh) + '_wv_' + str(wv) + '#' + unk_id + '.dat')

                    output_fpath = os.path.join(output_dirpath,
                                                'output_wh_' + str(wh) + '_wv_' + str(wv) + '#' + unk_id + '.csv')

                    with open(ref_fpath, 'w') as ref_file:
                        ref_file.write(ref_file_string)
                        ref_file.close()

                    with open(unk_fpath, 'w') as unk_file:
                        unk_file.write(unk_file_string)
                        unk_file.close()

                    batch_string_ = string_to_batch_file(cal_exec_file, ref_file=ref_fpath,
                                                         cal_input_file=unk_fpath,
                                                         output_file=output_fpath,
                                                         wave_height=wh,
                                                         wave_velocity=wv,
                                                         length=length,
                                                         pressure=pressure,
                                                         temperature=temperature,
                                                         lambda_=lambda_,
                                                         accuracy=accuracy,
                                                         fix_params=fix_params)

                    batch_string += batch_string_

                if uniq_spec.startswith('PolyAla'):
                    charge_range = [1, 2, 3]

                    for charge in charge_range:
                        unk_file_string = ''
                        ref_file_string = ''
                        unk_id = 'Polyala_'+str(charge)
                        for num in range(len(all_ion_file_read)):
                            if all_ion_file_read[num].startswith('PolyAla'):
                                if int(all_ion_file_read[num].split()[2]) == charge:
                                    unk_file_string += all_ion_file_read[num] + '\n'
                                else:
                                    ref_file_string += all_ion_file_read[num] + '\n'
                            else:
                                ref_file_string += all_ion_file_read[num] + '\n'

                        ## write file in this tab

                        ref_fpath = os.path.join(ref_dirpath,
                                                 'ref_wh_' + str(wh) + '_wv_' + str(wv) + '#' + unk_id + '.dat')
                        unk_fpath = os.path.join(unk_dirpath,
                                                 'unk_wh_' + str(wh) + '_wv_' + str(wv) + '#' + unk_id + '.dat')

                        output_fpath = os.path.join(output_dirpath,
                                                    'output_wh_' + str(wh) + '_wv_' + str(wv) + '#' + unk_id + '.csv')

                        with open(ref_fpath, 'w') as ref_file:
                            ref_file.write(ref_file_string)
                            ref_file.close()

                        with open(unk_fpath, 'w') as unk_file:
                            unk_file.write(unk_file_string)
                            unk_file.close()

                        batch_string_ = string_to_batch_file(cal_exec_file, ref_file=ref_fpath,
                                                             cal_input_file=unk_fpath,
                                                             output_file=output_fpath,
                                                             wave_height=wh,
                                                             wave_velocity=wv,
                                                             length=length,
                                                             pressure=pressure,
                                                             temperature=temperature,
                                                             lambda_=lambda_,
                                                             accuracy=accuracy,
                                                             fix_params=fix_params)

                        batch_string += batch_string_

    with open(os.path.join(outdir, cal_batch_fname), 'w') as batch_file:
        batch_file.write(batch_string)
        batch_file.close()

    print('Finished making files!')

    if run_cal:
        print('\nRunning calibration now... time starts now...')
        start_time = time.time()
        subprocess.run(os.path.join(outdir, cal_batch_fname))
        print('\n Finished! It took blimey ', round((time.time() - start_time)/60, 2), ' minutes')


if __name__ == '__main__':

    all_ion_dpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\norad\peptide\all\ref_input"
    outdir = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\norad\peptide\leaveonespec"
    cal_exec_file = os.path.join(os.getcwd(), r"IMSCal\bin\TWaveCalibrate")
    fix_a_ = [['a', 1]]
    fix_c_ = [['c', 0]]
    fix_a_c_ = [['a', 1], ['c', 0]]
    fix_c_scale_a = [['c', 0], ['scale_a']]
    scale_a = [['scale_a']]
    fix_t = [['t0', 0.00055]]
    scale_a_fix_t = [['scale_a'], ['t0', 0.00055]]
    fix_t_fix_c = [['t0', 0.00055], ['c', 0]]
    scale_a_fix_t_fix_c = [['t0', 0.00055], ['c', 0], ['scale_a']]
    make_input_files_and_run_calibration(all_ion_dirpath=all_ion_dpath, outdir=outdir, cal_exec_file=cal_exec_file,
                                         cal_batch_fname='TwaveCal_batchfile.bat', pressure=3.4, temperature=298,
                                         accuracy=3, run_cal=False, fix_params=fix_c_scale_a)