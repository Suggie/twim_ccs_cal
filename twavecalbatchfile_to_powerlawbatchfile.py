import os
from aux_scripts import create_dirpath


def write_power_law_batch_file(dirpath, output_dir_path, radial=True):
    """
    write power law batch file based on Twavecal_batchfile.bat
    :param dirpath: directory where twave cal batchfile is
    :param radial: radial true or false
    :return: None
    """

    twave_cal_batchfile_path = os.path.join(dirpath, 'TwaveCal_batchfile.bat')
    power_law_batchfile_path = os.path.join(dirpath, 'PowerLaw_batchfile.txt')
    output_dirpath = create_dirpath(os.path.join(dirpath, output_dir_path))

    new_batch_file_string = ''
    with open(twave_cal_batchfile_path, 'r') as twave_batch_file:
        twave_batch_file_read = twave_batch_file.read().splitlines()
        for ind, batch_line in enumerate(twave_batch_file_read):
            chars = batch_line.split('"')
            ref_fpath = chars[1]
            unk_fpath = chars[3]
            out_fpath = chars[5]
            out_fpath_split = os.path.split(out_fpath)
            output_fpath_new = os.path.join(output_dirpath, out_fpath_split[1])
            if radial:
                line = '-ref "{}" -input "{}" -output "{}" -radial\n'.format(ref_fpath, unk_fpath, output_fpath_new)
            else:
                line = '-ref "{}" -input "{}" -output "{}"\n'.format(ref_fpath, unk_fpath, output_fpath_new)
            new_batch_file_string += line

    with open(power_law_batchfile_path, 'w') as outfile:
        outfile.write(new_batch_file_string)
        outfile.close()

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\rad\_subclass\all"
    write_power_law_batch_file(dirpath, output_dir_path='PowerLaw', radial=False)